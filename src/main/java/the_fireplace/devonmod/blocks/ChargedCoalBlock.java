package the_fireplace.devonmod.blocks;

import the_fireplace.devonmod.DevonModBase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.init.Blocks;
import net.minecraft.world.IBlockAccess;
import net.minecraftforge.common.util.ForgeDirection;

public class ChargedCoalBlock extends Block{

	public ChargedCoalBlock(Material p_i45394_1_) {
		super(p_i45394_1_);
		setCreativeTab(DevonModBase.TabDevonMod);
		setBlockTextureName("devonmod:ChargedCoalBlock");
		setBlockName("chargedCoalBlock");
		setHardness(5.0F);
		setResistance(10.0F);
		setStepSound(soundTypePiston);
	}
	@Override
	public boolean canProvidePower()
    {
        return true;
    }
	@Override
	public int getFlammability(IBlockAccess world, int x, int y, int z, ForgeDirection face)
    {
        return 125;
    }
	@Override
	public int getFireSpreadSpeed(IBlockAccess world, int x, int y, int z, ForgeDirection face)
    {
        return 10;
    }
}
