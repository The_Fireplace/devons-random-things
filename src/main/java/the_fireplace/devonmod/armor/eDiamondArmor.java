package the_fireplace.devonmod.armor;

import the_fireplace.devonmod.DevonModBase;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.ItemStack;

public class eDiamondArmor extends ItemArmor{

	public eDiamondArmor(ArmorMaterial par2ArmorMaterial,
            int par3, int par4, String armornamePrefix)
{
super(par2ArmorMaterial, par3, par4);
this.material = par2ArmorMaterial;
par2ArmorMaterial.getDamageReductionAmount(par4);
this.setMaxDamage(par2ArmorMaterial.getDurability(par4));
maxStackSize = 1;
armorNamePrefix = armornamePrefix;
}
public String armorNamePrefix;
public ArmorMaterial material;
@Override
public boolean getIsRepairable(ItemStack tool, ItemStack material) {
	return material == new ItemStack(Items.emerald);
}
public String getArmorTexture(ItemStack stack, Entity entity, int slot, String type) {

    if(stack.getItem() == DevonModBase.eDiamondHelmet || stack.getItem() == DevonModBase.eDiamondChestplate || stack.getItem() == DevonModBase.eDiamondBoots) {

            return "devonmod:textures/models/armor/e_diamond_layer_1.png";

    }

    if(stack.getItem() == DevonModBase.eDiamondLeggings) {

            return "devonmod:textures/models/armor/e_diamond_layer_2.png";

    }

    else return null;
}

}
