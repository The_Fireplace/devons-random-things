package the_fireplace.devonmod.items;

import the_fireplace.devonmod.DevonModBase;
import the_fireplace.devonmod.entities.EntityChargedCoal;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;

public class CoalGun extends Item{
	public CoalGun(){
		setCreativeTab(DevonModBase.TabDevonMod);
		setTextureName("devonmod:Coalgun");
		setUnlocalizedName("CoalGun");
        setMaxStackSize(1);
	}
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
    {
        if (par3EntityPlayer.capabilities.isCreativeMode || par3EntityPlayer.inventory.consumeInventoryItem(DevonModBase.ChargedCoal))
        {
            par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 1.5F, 0.001F / (itemRand.nextFloat() * 0.00001F + 0.00001F));

            if (!par2World.isRemote)
            {
                par2World.spawnEntityInWorld(new EntityChargedCoal(par2World, par3EntityPlayer));
            }
        }

        return par1ItemStack;
    }
}
