package the_fireplace.devonmod.paxels;

import java.util.Set;

import the_fireplace.devonmod.DevonModBase;
import the_fireplace.fireplacecore.tools.ItemPaxel;
import net.minecraft.client.renderer.texture.IIconRegister;

public class StonePaxel extends ItemPaxel{

	public StonePaxel(ToolMaterial par2ToolMaterial, Set field_150914_c) {
		super(par2ToolMaterial, field_150914_c);
        setMaxStackSize(1);
        setCreativeTab(DevonModBase.TabDevonMod);
        setUnlocalizedName("stonePaxel");
	}
	@Override
    public void registerIcons(IIconRegister par1IconRegister) {
            itemIcon = par1IconRegister.registerIcon("devonmod:stone_paxel");
    }

}
